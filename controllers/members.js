const express = require("express");
const Member = require("../models/member");
// const Copy = require("../models/copy");

function list(req, res, next) {
  Member.find()
    .populate()
    .then((objs) =>
      res.status(200).json({
        message: "Lista de miembros",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error en la lista de miembros",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;
  Member.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Miembro encontrado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al buscar miembro`,
        obj: ex,
      })
    );
}

function create(req, res, next) {
  const name = req.body.name;
  const lastName = req.body.lastName;
  const phone = req.body.phone;

  let address = new Object();
  address.street = req.body.street;
  address.number = req.body.number;
  address.zip = req.body.zip;
  address.state = req.body.state;

  // address = req.body.address;

  let member = new Member({
    name: name,
    lastName: lastName,
    address: address,
    phone: phone,
  });

  member
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Miembro credo existosamente",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error al crear miembro",
        obj: ex,
      })
    );
}

function replace(req, res, next) {
  const id = req.params.id;
  let name = req.body.name ? req.body.name : "";
  let lastName = req.body.lastName ? req.body.lastName : "";
  let phone = req.body.phone ? req.body.phone : "";

  let address = new Object();
  address.street = req.body.street ? req.body.street : "";
  address.number = req.body.number ? req.body.number : "";
  address.zip = req.body.zip ? req.body.zip : 0;
  address.state = req.body.state ? req.body.state : "";

  let member = new Member({
    _name: name,
    _lastName: lastName,
    _address: address,
    _phone: phone,
  });

  Member.findOneAndUpdate({ _id: id }, member, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Miembro remplazado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al remplazar miembro`,
        obj: ex,
      })
    );
}

async function update(req, res, next) {
  const id = req.params.id;
  let name = req.body.name;
  let lastName = req.body.lastName;
  let address = req.body.address;
  let phone = req.body.phone;

  let member = new Object();

  if (name) member._name = name;
  if (lastName) member._lastName = lastName;
  if (address) member._address = address;
  if (phone) member._phone = phone;

  Member.findOneAndUpdate({ _id: id }, member, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Miembro actualizado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Erro al actualizar miembro`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Member.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Miembro borrado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al borrar miembro`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy };
