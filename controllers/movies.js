const express = require("express");
const Movie = require("../models/movie");
const Genre = require("../models/genre");
const Director = require("../models/director");
const Actor = require("../models/actor");

function list(req, res, next) {
  Movie.find()
    .populate(["_genre", "_director","_cast"])
    .then((objs) =>
      res.status(200).json({
        message: "Lista de peliculas",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error en la lista de peliculas",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  Movie.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Pelicula encontrada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al buscar la pelicula`,
        obj: ex,
      })
    );
}

async function create(req, res, next) {
  const title = req.body.title;
  const genreId = req.body.genreId;
  const directorId = req.body.directorId;
  // const castId = req.body.castId;

  let genre = null;
  let director = null;
  let cast = [];

  if(genreId) genre = await Genre.findOne({ _id: genreId });
  if(directorId) director = await Director.findOne({ _id: directorId });
  // cast = await Actor.find({ _id: castId });

  let movie = new Movie({
    _title: title,
    _genre: genre,
    _director: director,
    _cast: [],
  });

  movie
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Pelicula creada existosamente",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error al crear pelicula",
        obj: ex,
      })
    );
}

async function addActor(req, res, next) {
  const id = req.params.id;
  const actorId = req.body.actorId;

  const actor = await Actor.findOne({_id:actorId});

  Movie.findByIdAndUpdate(
    { _id: id },
    {$push: {"_cast": actor}},
    {upsert: true, new : true})
    .then((obj) =>
      res.status(200).json({
        message: `Se actualizo la lista de actores correctamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar lista de actores`,
        obj: ex,
      })
    );
}

async function removeActor(req, res, next) {
  const id = req.params.id;
  const actorId = req.body.actorId;
  const castId = req.body.castId;

  const actor = await Actor.findOne({_id:actorId});

  Movie.findByIdAndUpdate(
    { _id: id },
    {$pull: {"_cast": actor}},
    {upsert: true, new : true})
    .then((obj) =>
      res.status(200).json({
        message: `Se actualizo la lista de actores correctamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar lista de actores`,
        obj: ex,
      })
    );
}

async function replace(req, res, next) {
  const id = req.params.id;
  let title = req.body.title ? req.body.title : "";
  let genreId = req.body.genreId ? req.body.genreId : "";
  let directorId = req.body.directorId ? req.body.directorId : "";
  let castId = req.body.castId ? req.body.castId : [];

  let genre = await Genre.findOne({ _id: genreId });
  let director = await Director.findOne({ _id: directorId });
  let cast = await Actor.find({ _id: castId });

  let movie = new Object({
    _title: title,
    _genre: genre,
    _director: director,
    _cast: cast,
  });

  Movie.findOneAndUpdate({ _id: id }, movie, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Pelicula remplazada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al replazar pelicula`,
        obj: ex,
      })
    );
}

async function update(req, res, next) {
  const id = req.params.id;

  let title = req.body.title;
  let genreId = req.body.genreId;
  let directorId = req.body.directorId;
  let castId = req.body.castId;

  let genre = undefined;
  let director = undefined;
  let cast = undefined;

  if(genreId) genre = await Genre.findOne({ _id: genreId });
  if(directorId) director = await Director.findOne({ _id: directorId });
  if(cast) cast = await Actor.find({ _id: castId });

  console.log(director);

  let movie = new Object();

  if (title) movie._title = title;
  if (genre) movie._genre = genre;
  if (director) movie._director = director;
  if (cast) movie._cast = cast;

  Movie.findOneAndUpdate({ _id: id }, movie, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Pelicula actualizada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar pelicula`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Movie.deleteOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Pelicula borrada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al borrar pelicula`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy, addActor,removeActor };
