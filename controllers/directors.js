const express = require('express');
const Director = require('../models/director');

function list(req, res, next) {
    Director.find()
        .then(obj => res.status(200).json({
            message: "Lista de directores",
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: "Error con la lista de directores",
            obj: ex
        }));
}

function index(req, res, next) {
    const id = req.params.id;
    
    Director.findOne({"_id": id})
        .then(obj => res.status(200).json({
            message: `Director encontrado exitosamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al buscar director`,
            obj: ex
        }));
}

function create(req, res, next) {
    const name = req.body.name;
    const last_name = req.body.last_name;

    let director = new Director({
        name: name,
        last_name: last_name
    });

    director.save()
        .then(obj => res.status(200).json({
            message: "Director creado correctamente",
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: "No se pudo crear el director",
            obj: ex
        }));
    
}

function replace(req, res, next) {
    const id = req.params.id;

    let name = req.body.name ? req.body.name : "";
    let last_name = req.body.last_name ? req.body.last_name : "";
    
    let director = new Object({
        _name: name,
        _last_name: last_name
    });

    Director.findOneAndUpdate({"_id":id},director,{new: true})
        .then(obj => res.status(200).json({
            message: `Director remplazado correctamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al rempalzar director`,
            obj: ex
        }));
}

function update(req, res, next) {
    const id = req.params.id;
    
    let name = req.body.name;
    let last_name = req.body.last_name;
    
    let direction = new Object();

    if(name) direction._name = name;
    if(last_name) direction._last_name = last_name;

    Director.findOneAndUpdate({"_id":id},direction,{new:true})
        .then(obj => res.status(200).json({
            message: `Director actualizado correctamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al actualizar director`,
            obj: ex
        }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    
    Director.deleteOne({"_id": id})
        .then(obj => res.status(200).json({
            message: `Director borrado correctamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al borrar director`,
            obj: ex
        }))
}

module.exports = { list, index, create, replace, update, destroy };