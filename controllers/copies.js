const express = require("express");
const Copy = require("../models/copy");
const Movie = require("../models/movie");

function list(req, res, next) {
  Copy.find()
    .populate("_movie")
    .then((objs) =>
      res.status(200).json({
        message: "Lista de copias",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error con la lista de copias",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;
  Copy.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Copias encontradas existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al encontrar copias`,
        obj: ex,
      })
    );
}

async function create(req, res, next) {
  const number = req.body.number;
  const format = req.body.format;
  const movieId = req.body.movieId;
  const status = req.body.status;

  let movie = await Movie.findOne({ _id: movieId });

  let copy = new Copy({
    _movie: movie,
    _number: number,
    _format: format,
    _status: status,
  });

  copy
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Copia creada existosamente",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error al crear copia",
        obj: ex,
      })
    );
}

async function replace(req, res, next) {
  const id = req.params.id;
  let number = req.body.number ? req.body.number : -1;
  let format = req.body.format ? req.body.format : "";
  let movieId = req.body.movieId ? req.body.movieId : "";
  let status = req.body.status ? req.body.status : "";

  let movie = await Movie.findOne({ _id: movieId });

  let copy = new Copy({
    _movie: movie,
    _number: number,
    _format: format,
    _status: status,
  });

  Copy.findOneAndUpdate({ _id: id }, copy, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Copia remplazada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al remplazar copia`,
        obj: ex,
      })
    );
}

async function update(req, res, next) {
  const id = req.params.id;
  let number = req.body.number;
  let format = req.body.format;
  let movieId = req.body.movieId;
  let status = req.body.status;

  let movie = await Movie.findOne({ _id: movieId });

  let copy = new Object();

  if (number) copy._number = number;
  if (format) copy._format = format;
  if (movie) copy._movie = movie;
  if (status) copy._status = status;

  Copy.findOneAndUpdate({ _id: id }, copy, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Copia actualizada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar copia`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Copia borrada existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al borrar copia`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy };
