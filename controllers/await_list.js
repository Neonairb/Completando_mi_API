const express = require('express');
const AwaitList = require('../models/await_list');
const Member = require('../models/member');
const Movie = require('../models/movie');

function list(req, res, next) {
    AwaitList.find().populate(["_member", "_movie"])
        .then(objs => res.status(200).json({
            message: "Lista de espera",
            obj: objs
        }))
        .catch(ex => res.status(500).json({
            message: "Error al crear lista de espera",
            obj: ex
        }));
}

function index(req, res, next) {
    const id = req.params.id;
    AwaitList.findOne({"_id": id})
        .then(obj => res.status(200).json({
            message: `Se econtro la lista de espera exitosamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al buscar al lista de espera`,
            obj: ex
        }));
}

async function create(req, res, next) {
    const memberId = req.body.memberId;
    const movieId = req.body.movieId;

    let member = await Member.findOne({"_id": memberId});
    let movie = await Movie.findOne({"_id": movieId});

    let awaitList = new AwaitList({
        member: member,
        movie: movie
    });
    
    awaitList.save()
        .then(obj => res.status(200).json({
            message: "Lista de espera creada existosamente",
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: "Error al crear la lista de espera",
            obj: ex
        }));
}

async function replace(req, res, next) {
    const id = req.params.id;
    let memberId = req.body.memberId ? req.body.memberId: "";
    let movieId = req.body.movieId ? req.body.movieId: "";

    let member = await Member.findOne({"_id": memberId});
    let movie = await Movie.findOne({"_id": movieId});

    let awaitList = new Object({
        _member: member,
        _movie: movie
    });

    AwaitList.findOneAndUpdate({"_id": id}, awaitList, { new: true })
        .then(obj => res.status(200).json({
            message: `Lista de espera actualizada existosamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al remplazar la Lista de espera`,
            obj: ex
        }));
}

async function update(req, res, next) {
    const id = req.params.id;
    let memberId = req.body.memberId;
    let movieId = req.body.movieId;

    let member = await Member.findOne({"_id": memberId});
    let movie = await Movie.findOne({"_id": movieId});

    let awaitList = new Object();

    if(member) awaitList._member = member;
    if(movie) awaitList._movie = movie;

    AwaitList.findOneAndUpdate({"_id": id}, awaitList, { new: true })
        .then(obj => res.status(200).json({
            message: `Lista de espera actualizada existosamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al actualizar la lista de espera`,
            obj: ex
        }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    AwaitList.remove({"_id": id})
        .then(obj => res.status(200).json({
            message: `Lista de espera borrada existosamente`,
            obj: obj
        }))
        .catch(ex => res.status(500).json({
            message: `Error al borrar la lista de espera`,
            obj: ex
        }));
}

module.exports = { list, index, create, replace, update, destroy };
