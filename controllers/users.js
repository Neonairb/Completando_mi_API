const express = require("express");
const bcrypt = require("bcrypt");
const User = require("../models/user");

function list(req, res, next) {
  User.find()
    .then((objs) =>
      res.status(200).json({
        message: "Lista de usuarios",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error con la lista de usuarios",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;
  User.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Usuario encontrado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al buscar usuario`,
        obj: ex,
      })
    );
}

async function create(req, res, next) {
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;

  const salt = await bcrypt.genSalt(10);
  const passwordHash = await bcrypt.hash(password, salt);

  let user = new User({
    name: name,
    lastName: lastName,
    email: email,
    password: passwordHash,
    salt: salt,
  });

  user
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Usuario creado existosamente",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "Error al crear usuario",
        obj: ex,
      })
    );
}

async function replace(req, res, next) {
  const id = req.params.id;
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : "";
  const email = req.body.email ? req.body.email : "";
  const password = req.body.password ? req.body.password : "";

  const salt = await bcrypt.genSalt(10);
  const passwordHashGen = await bcrypt.hash(password, salt);

  let user = new Object({
    _name: name,
    _lastName: lastName,
    _email: email,
    _password: passwordHashGen,
    _salt: salt,
  });

  User.findOneAndUpdate({ _id: id }, user, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Usuario remplazado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al remplazar usuario`,
        obj: ex,
      })
    );
}

async function update(req, res, next) {
  const id = req.params.id;
  const name = req.body.name;
  const lastName = req.body.lastName;
  const email = req.body.email;
  const password = req.body.password;
  const salt = await bcrypt.genSalt(10);

  const passwordHashGen = await bcrypt.hash(password, salt);

  let user = new Object();

  if (name) user._name = name;
  if (lastName) user._lastName = lastName;
  if (email) user._email = email;
  if (password) user._password = passwordHashGen;
  if (salt) user._salt = salt;

  User.findOneAndUpdate({ _id: id }, user, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Objeto actualizado correctamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar objeto`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  User.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Usuario borrado existosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al borrar usuario`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy };
