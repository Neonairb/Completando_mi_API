const express = require("express");
const Address = require("../models/address");

function list(req, res, next) {
    Address.find()
    .populate()
    .then((objs) =>
      res.status(200).json({
        message: "Lista de direcciones",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "No se puedo crear la lista de direcciones",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;
  Address.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Se encontro la direccion`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `No se encntro la direccion`,
        obj: ex,
      })
    );
}

async function create(req, res, next) {
  const street = req.body.street;
  const number = req.body.number;
  const zip = req.body.zip;
  const state = req.body.state;

  let address = new Address({
    _street: street,
    _number: number,
    _zip: zip,
    _state: state
  });

  address
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Se creo la direccion correctamente",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "No se pudo crear la direccion correctamente",
        obj: ex,
      })
    );
}

async function replace(req, res, next) {
  const id = req.params.id;
  const street = req.body.street ? req.body.street : "";
  const number = req.body.number ? req.body.number : "";
  const zip = req.body.zip ? req.body.zip : 0;
  const state = req.body.state ? req.body.state : "";

  let address = new Address({
    _street: street,
    _number: number,
    _zip: zip,
    _state: state
  });

  Address.findOneAndUpdate({ _id: id }, address, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Se pudo remplazar la direccion correctamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `No se pudo remplazar la direccion`,
        obj: ex,
      })
    );
}

async function update(req, res, next) {
  const id = req.params.id;
  const street = req.body.street;
  const number = req.body.number;
  const zip = req.body.zip;
  const state = req.body.state;

  let address = new Object();

  if (street) address._street = street;
  if (number) address._number = number;
  if (zip) address._zip = zip;
  if (state) address._state = state;


  Address.findOneAndUpdate({ _id: id }, address, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Direccion actualizada exitosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al actualizar direccion`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;
  Address.remove({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Dreccion borrada exitosamente`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al borrar direccion`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy };
