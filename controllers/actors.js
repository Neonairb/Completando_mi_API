const express = require("express");
const Actor = require("../models/actor");

function list(req, res, next) {
  Actor.find()
    .then((objs) =>
      res.status(200).json({
        message: "Lista de Actores",
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "No se encontro la lista de actores",
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  Actor.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Se encontro el actor con el id: ${id}`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `No se econtro actor con id: ${id}`,
        obj: ex,
      })
    );
}

function create(req, res, next) {
  const name = req.body.name;
  const last_name = req.body.last_name;

  let actor = new Actor({
    _name: name,
    _lastName: last_name,
  });

  actor
    .save()
    .then((obj) =>
      res.status(200).json({
        message: "Actor creado correctamene",
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: "No se pudo crear el actor",
        obj: ex,
      })
    );
}

function replace(req, res, next) {
  const id = req.params.id;

  let name = req.body.name ? req.body.name : "";
  let last_name = req.body.last_name ? req.body.last_name : "";

  let actor = new Object({
    _name: name,
    _lastName: last_name,
  });

  Actor.findOneAndUpdate({ _id: id }, actor, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Actor remplazado correctamente: ${id}`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `Error al remplazar al actor: ${id}`,
        obj: ex,
      })
    );
}

function update(req, res, next) {
  const id = req.params.id;

  let name = req.body.name;
  let last_name = req.body.last_name;

  let actor = new Object();

  if (name) actor._name = name;
  if (last_name) actor._lastName = last_name;

  Actor.findOneAndUpdate({ _id: id }, actor, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: `Actor actualizado correctamente: ${id}`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `No se pudo actualizar el actor: ${id}`,
        obj: ex,
      })
    );
}

function destroy(req, res, next) {
  const id = req.params.id;

  Actor.deleteOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: `Se borro correctamente el actor con id: ${id}`,
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: `No se pudo borrar el actor con id: ${id}`,
        obj: ex,
      })
    );
}

module.exports = { list, index, create, replace, update, destroy };
