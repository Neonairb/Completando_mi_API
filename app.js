const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const { expressjwt } = require('express-jwt');

const indexRouter = require('./routes/index');
const genresRouter = require('./routes/genres');
const directorRouter = require('./routes/directors');
const movieRouter = require('./routes/movies');
const actorRouter = require('./routes/actors');
const membersRouter = require('./routes/members');
const usersRouter = require('./routes/users');
const copiesRouter = require('./routes/copies');
const addressRouter = require('./routes/address');
const awaitListRouter = require('./routes/awaitList');


// CONECTION DATABSE "mongodb://<dbUser>?:<dbPassword>?@<direction>:<port>/<dbName>"
const uri = "mongodb://localhost:27017/videoClub";

mongoose.connect(uri);
const db = mongoose.connection;


const app = express();

db.on('open', () => {
  console.log("Conexion Exitosa")
});

db.on('error',() => {
  console.log("Erro de conexion")
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const jwtKey = "8Zz5tw0Ionm3XPZZfN0NOml3z9FMfmpgXwovR9fp6ryDIoGRM8EPHAB6iHsc0fb";
app.use(expressjwt({secret: jwtKey, algorithms: ['HS256']})
  .unless({path: ['/login']}));


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/genres', genresRouter);
app.use('/directors',directorRouter);
app.use('/movies',movieRouter);
app.use('/actors',actorRouter);
app.use('/members',membersRouter);
app.use('/copies',copiesRouter);
app.use('/address',addressRouter);
app.use('/awaitlist',awaitListRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
