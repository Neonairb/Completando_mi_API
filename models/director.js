const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _lastname: String,
    _movieId: String
});

class Director {
    constructor(name, lastname, movieId) {
        this._name = name;
        this._lastname = lastname;
        this._movieId = movieId
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get lastname() {
        return this._lastname;
    }

    set lastname(lastname) {
        this._lastname = lastname;
    }

    get movieId() {
        return this._movieId;
    }

    set movieId(movieId) {
        this._movieId = movieId;
    }
}


schema.loadClass(Director);

module.export = mongoose.model('Director', schema);