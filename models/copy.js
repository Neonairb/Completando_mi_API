const mongoose = require('mongoose');

const schema = mongoose.Schema({
  _movie: {type: mongoose.Schema.ObjectId, ref: 'Movie'},
  _number: Number,
  _format: { type: String, enum: ['VHS','DVD','BLU_RAY'] },
  _status: { type: String, enum: ['RENTED','AVAILABLE'] }
});

class Copy {
  constructor(format, movie, number, status) {
    this._movie = movie;
    this._number = number;
    this._format = format;
    this._status = status;
  }

  get movie(){
    return this._movie;
  }
  set movie(v){
    this._movie = v;
  }

  get number(){
    return this._number;
  }
  set number(v){
    this._number = v;
  }
  
  get format(){
    return this._format;
  }
  set format(v){
    this._format = v;
  }
  
  get status(){
    return this._status;
  }
  set status(v){
    this._status = v;
  }
}

schema.loadClass(Copy);
module.exports = mongoose.model('Copy', schema);
