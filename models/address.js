const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _street: String,
    _number: String,
    _zip: Number,
    _state: String
});

class Adress {
    constructor(street, number, zip, state){
        this._street = street;
        this._number = number;
        this._zip = zip;
        this._state = state;
    }

    get street(){
        return this._street;
    }

    set street(v){
        this._street = v;
    }

    get number(){
        return this._number;
    }

    set number(v){
        this._number = v;
    }

    get zip(){
        return this._zip;
    }

    set zip(v){
        this._zip = v;
    }

    get state(){
        return this._state;
    }

    set state(v){
        this._state = v;
    }

}

schema.loadClass(Adress);
module.exports = mongoose.model('Address', schema);